// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Highlight"
{
	Properties
	{
		_Color("Color", Color) = (0.8382353,0.04930795,0.04930795,1)
		_Hardness("Hardness", Float) = 1
		_Thickness("Thickness", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Front
		ZWrite Off
		ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float4 screenPos;
			float eyeDepth;
			float3 worldPos;
			float3 vertexToFrag7_g46;
		};

		uniform float4 _Color;
		uniform sampler2D _CameraDepthTexture;
		uniform float _Thickness;
		uniform float _Hardness;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.eyeDepth = -UnityObjectToViewPos( v.vertex.xyz ).z;
			float4 ase_vertex4Pos = v.vertex;
			o.vertexToFrag7_g46 = ( (mul( UNITY_MATRIX_MV, ase_vertex4Pos )).xyz * float3(-1,-1,1) );
		}

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			o.Emission = _Color.rgb;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float eyeDepth14 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float4 transform3_g6 = mul(unity_ObjectToWorld,float4(0,0,0,1));
			float3 temp_output_69_0 = (transform3_g6).xyz;
			float3 ase_worldPos = i.worldPos;
			float4 transform50 = mul(unity_WorldToObject,float4( ( temp_output_69_0 - ase_worldPos ) , 0.0 ));
			float4 transform51 = mul(unity_WorldToObject,float4( ( temp_output_69_0 - _WorldSpaceCameraPos ) , 0.0 ));
			float4 temp_output_5_0_g47 = ( ( ( ( ( eyeDepth14 / i.eyeDepth ) * ( transform50 - transform51 ) ) + transform51 ) - float4( float3(0,0,0) , 0.0 ) ) / 0.5 );
			float dotResult19_g47 = dot( temp_output_5_0_g47 , temp_output_5_0_g47 );
			float clampResult10_g47 = clamp( dotResult19_g47 , 0 , 1 );
			float4 transform3_g45 = mul(unity_ObjectToWorld,float4(0,0,0,1));
			float clampDepth11_g46 = Linear01Depth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float4 appendResult15_g46 = (float4(( clampDepth11_g46 * ( i.vertexToFrag7_g46 * ( _ProjectionParams.z / (i.vertexToFrag7_g46).z ) ) ) , 1.0));
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			o.Alpha = ( _Color.a * ( ( 1 - pow( clampResult10_g47 , 200.0 ) ) * saturate( ( pow( max( ( distance( transform3_g45 , mul( unity_CameraToWorld, appendResult15_g46 ) ) - ( ( 0.5 * ase_objectScale.x ) - _Thickness ) ) , 0 ) , _Hardness ) / _Thickness ) ) ) );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15101
8;1034;1906;1103;2971.256;-326.9205;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;101;-2709.643,605.1275;Float;False;1876.734;633.1835;It does something like what I want?? LOL;11;132;130;125;126;123;103;102;136;104;141;158;Center Cutout;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;99;-2706.385,1.339306;Float;False;1538.832;586.6199;Determines where the mask should be applied;16;17;45;12;11;46;15;14;51;13;50;7;8;6;1;69;37;Mask Position;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;136;-2702.814,956.778;Float;False;392.6655;272.5438;Object Radius;3;135;112;134;;1,1,1,1;0;0
Node;AmplifyShaderEditor.FunctionNode;37;-2688.385,337.8949;Float;False;Actor Position;-1;;6;e71700e2930130546b76e9cf36cffc1d;0;0;1;FLOAT4;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;6;-2611.626,423.284;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ObjectScaleNode;112;-2674.914,1097.122;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;134;-2672.871,998.978;Float;False;Constant;_ObjectRadius;Object Radius;4;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;69;-2485.951,332.9257;Float;False;True;True;True;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;1;-2546.778,167.9266;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.FunctionNode;102;-2688,675;Float;False;Actor Position;-1;;45;112308af066c30749bf508fdc96e58fa;0;0;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;104;-2571.499,860.9;Float;False;Property;_Thickness;Thickness;3;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;135;-2481.749,1039.165;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;8;-2144.991,216.1751;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;170;-2715.938,762.1804;Float;False;Reconstruct World Position From Depth;-1;;46;e7094bcbcc80eb140b2a3dbe6a861de8;0;1;21;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;7;-2140.109,422.8395;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DistanceOpNode;103;-2293.5,673.7001;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;141;-2291.872,780.8937;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldToObjectTransfNode;51;-1973.139,423.2349;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldToObjectTransfNode;50;-1968.139,217.2352;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SurfaceDepthNode;13;-1950.647,116.2735;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenDepthNode;14;-1949.473,51.33931;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;11;-1684.393,301.4283;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;46;-1522.5,433.0449;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;123;-2104.249,687.0771;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;15;-1674.114,92.64741;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;100;-1157.706,2.742514;Float;False;820.4473;581.0168;Performs the actual intersection masking;5;28;53;30;96;98;Masking;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;158;-1898.429,690.2354;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-1511.016,182.1498;Float;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;126;-1816.141,803.3054;Float;False;Property;_Hardness;Hardness;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;45;-1385.834,390.792;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.PowerNode;125;-1611.009,686.278;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-1293.154,184.2878;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-1106.853,433.6833;Float;False;Constant;_Radius;Radius;2;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;30;-1114.981,291.8844;Float;False;Constant;_Vector1;Vector 1;3;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;53;-1104.727,503.922;Float;False;Constant;_MaskHardness;Mask Hardness;1;0;Create;True;0;0;False;0;200;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;130;-1342.339,691.6774;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;98;-840.8812,173.9939;Float;False;Sphere Mask New;-1;;47;e0315a3b9e99b3e4ab8cb9813b2fba90;0;4;16;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT;0;False;12;FLOAT;100;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;96;-489.4327,163.8125;Float;False;2;0;FLOAT;1;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;132;-1067.598,686.0284;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;21;-249.7658,14.86872;Float;False;Property;_Color;Color;1;0;Create;True;0;0;False;0;0.8382353,0.04930795,0.04930795,1;0.8382353,0.04930795,0.04930795,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;133;-297.3992,198.9285;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;171;-132.256,204.9205;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;92;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Highlight;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Front;2;False;-1;7;False;-1;False;0;0;False;5;Custom;0.5;True;False;0;True;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;False;0;0;0;False;-1;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;69;0;37;0
WireConnection;135;0;134;0
WireConnection;135;1;112;1
WireConnection;8;0;69;0
WireConnection;8;1;1;0
WireConnection;7;0;69;0
WireConnection;7;1;6;0
WireConnection;103;0;102;0
WireConnection;103;1;170;0
WireConnection;141;0;135;0
WireConnection;141;1;104;0
WireConnection;51;0;7;0
WireConnection;50;0;8;0
WireConnection;11;0;50;0
WireConnection;11;1;51;0
WireConnection;46;0;51;0
WireConnection;123;0;103;0
WireConnection;123;1;141;0
WireConnection;15;0;14;0
WireConnection;15;1;13;0
WireConnection;158;0;123;0
WireConnection;12;0;15;0
WireConnection;12;1;11;0
WireConnection;45;0;46;0
WireConnection;125;0;158;0
WireConnection;125;1;126;0
WireConnection;17;0;12;0
WireConnection;17;1;45;0
WireConnection;130;0;125;0
WireConnection;130;1;104;0
WireConnection;98;16;17;0
WireConnection;98;15;30;0
WireConnection;98;14;28;0
WireConnection;98;12;53;0
WireConnection;96;1;98;0
WireConnection;132;0;130;0
WireConnection;133;0;96;0
WireConnection;133;1;132;0
WireConnection;171;0;21;4
WireConnection;171;1;133;0
WireConnection;92;2;21;0
WireConnection;92;9;171;0
ASEEND*/
//CHKSM=26578C930AE02AF54D355360107E5E5BF39C7025