﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayScript : MonoBehaviour {
    public GameObject PauseMenu;
    
    private void OnTriggerEnter(Collider col)
    {
        Debug.Log("Contunie Collision detected");
        PauseMenu.GetComponent<PauseMenu>().Resume();
    }
}
