﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlussScript : MonoBehaviour
{
    public GameObject Slider;
    public float amount;

    private void OnTriggerEnter(Collider other)
    {
        Slider.GetComponent<Slider>().value += amount;
    }
}
