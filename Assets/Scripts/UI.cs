﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour {

    public Image background;
    public Color NormalColor;
    public Color HighlightColor;
    public GameObject levelSelector1;
    public GameObject levelSelector2;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        SceneManager.LoadScene("subway_Emmanuel");
        Debug.Log("it works");
    }

    public void StartGame2()
    {
        SceneManager.LoadScene("subway_Emmanuel");
    }

    public void SelectLevel1()
    {
        if (levelSelector2 == true)
        {
            levelSelector2.SetActive(false);
            levelSelector1.SetActive(true);
        }
        else
        {
            levelSelector1.SetActive(true);
        }
        
    }

    public void SelectLevel2()
    {
        if (levelSelector1 == true)
        {
            levelSelector1.SetActive(false);
            levelSelector2.SetActive(true);
        }
        else
        {
            levelSelector2.SetActive(true);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OnGazeEnter()
    {
        background.color = HighlightColor;
    }

    public void OnGazeExit()
    {
        background.color = NormalColor;
    }

    public void onClick()
    {
        Debug.Log("Click recieved");
    }
}
