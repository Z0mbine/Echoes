﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BaseNPC : MonoBehaviour, IBreakable
{
    [Header("General Settings")]
    [SerializeField]
    [Tooltip("The maximum health of this NPC")]
    private int maxHealth = 100;
    private int health = 100;

    public void TakeDamage(int amount)
    {
        health -= amount;

        if (health <= 0)
        {
            Kill();
        }
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    public int GetHealth()
    {
        return health;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public void SetHealth(int amount)
    {
        health = amount;
    }

    public void SetMaxHealth(int amount)
    {
        maxHealth = amount;
    }

    void Awake()
    {
        health = maxHealth;
    }

    void Start()
    {
		
	}
	
	void Update()
    {
		
	}
}
