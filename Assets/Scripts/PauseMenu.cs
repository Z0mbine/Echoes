﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;
    public GameObject PauseMenuUI;
    public GameObject SettingsMenu;
    public GameObject MainScreen;
    public Slider volumeSlider;
    public Rect SliderLocation;
    public float GammaCorrection;

    // Update is called once per frame
    void Update()
    {
        RenderSettings.ambientLight = new Color(GammaCorrection, GammaCorrection, GammaCorrection, 1.0f);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SettingButton()
    {
        PauseMenuUI.SetActive(false);
        SettingsMenu.SetActive(true);
    }

    public void SettingsButtonExit()
    {
        PauseMenuUI.SetActive(true);
        SettingsMenu.SetActive(false);
    }

    public void VolumeController()
    {
        AudioListener.volume = volumeSlider.value;
    }

    public void OnGUI()
    {
        GammaCorrection = GUI.HorizontalSlider(SliderLocation, GammaCorrection, 0, 1.0f);
    }
}
