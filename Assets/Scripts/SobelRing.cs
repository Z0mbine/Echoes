﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SobelRing : MonoBehaviour
{
    [HideInInspector]
    public float maxRadius = 0f;
    [HideInInspector]
    public float travelSpeed = 0f;
    [HideInInspector]
    public Color color;
    [HideInInspector]
    public float lifeTime = 0f;
    [HideInInspector]
    public float persistTime = 0f;
    [HideInInspector]
    public bool zTop = false;

    float startTime = 0f;
    float travelTime = 0f;
    float fadeOutStart = 0f;
    Renderer objRenderer;

    bool bStarted = false;
    bool bFading = false;

    [SerializeField]
    GameObject interior;
    Renderer intRenderer;
    
    float dist;

    void Start()
    {
        objRenderer = GetComponent<Renderer>();
    }

    /// <summary>
    /// Circular ease-in function, for sharp falloff.
    /// </summary>
    /// <param name="frac">Float from 0.0-1.0. Percent complete the interpolation is.</param>
    /// <param name="b">The beginning number.</param>
    /// <param name="c">How much to change by (end - beginning).</param>
    /// <returns>Eased number.</returns>
    private static float InCirc(float frac, float b, float c)
    {
        return(-c * (Mathf.Sqrt(1 - Mathf.Pow(frac, 2)) - 1) + b);
    }

    public void Init()
    {
        startTime = Time.time;
        travelTime = maxRadius / travelSpeed;

        intRenderer = interior.GetComponent<Renderer>();

        if (zTop)
            intRenderer.material.renderQueue = 4500;

        bStarted = true;
    }

    void OnTriggerEnter(Collider other)
    {
        IHighlight script = other.gameObject.GetComponent<IHighlight>();
        IReacting reactScript = other.gameObject.GetComponent<IReacting>();

        if (script != null)
        {
            script.Highlight(1f, color);
        }

        if (reactScript == null) return;

        reactScript.ReactSound(transform.position);

        if (dist < 0.15f)
        {
            reactScript.Find();
        }
    }

    void Update()
    {
        if (!bStarted)
            return;

        float elapsed = Time.time - startTime;
        float frac = Mathf.Min(elapsed / travelTime, 1f);
        float falloffFrac = InCirc(frac, 1, -1);
        
        dist = frac;

        transform.localScale = Vector3.one * (frac * maxRadius);
        objRenderer.material.SetColor("_Color", new Color(color.r, color.g, color.b, falloffFrac));

        if (frac < 1f || elapsed < persistTime)
        {
            intRenderer.material.SetColor("_EdgeColor", color);
            return;
        }

        if (!bFading)
        {
            fadeOutStart = Time.time;
            bFading = true;
        }

        float fadeElapsed = Time.time - fadeOutStart;
        float fadeFrac = Mathf.Min(fadeElapsed / lifeTime, 1f);

        intRenderer.material.SetColor("_EdgeColor", new Color(color.r, color.g, color.b, (1f - fadeFrac)));

        if (fadeFrac >= 1)
            Destroy(gameObject);
    }
}
