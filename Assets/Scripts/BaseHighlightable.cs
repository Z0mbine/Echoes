﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHighlightable : MonoBehaviour, IHighlight
{
    [SerializeField]
    Color highlightColor;

    [SerializeField]
    [Tooltip("Should we use this object's preferred color for highlights, or use function input?")]
    bool bPreferredColor;

    [SerializeField]
    float intensity = 1f;

    [SerializeField]
    float incrementSpeed = 0f;

    bool bHighlight = false;
    bool bIncrementing = false;
    float highlightStart = 0f;
    float highlightDuration = 0f;

    Renderer renderer;

    public void Highlight(float duration, Color color)
    {
        bHighlight = true;
        highlightStart = Time.time;
        highlightDuration = duration;
        bIncrementing = true;
        highlightColor = (bPreferredColor ? highlightColor : color);
    }

	void Start()
    {
        renderer = GetComponent<Renderer>();
	}
	
	void Update()
    {
        if (!bHighlight)
            return;

        //if (bIncrementing)
        //{
        //    Color newColor = new Color(
        //        Mathf.MoveTowards(curColor.r, highlightColor.r * intensity, incrementSpeed),
        //        Mathf.MoveTowards(curColor.g, highlightColor.g * intensity, incrementSpeed),
        //        Mathf.MoveTowards(curColor.b, highlightColor.b * intensity, incrementSpeed)
        //    );

        //    renderer.material.SetColor("_Color", newColor);

        //    if (newColor.r == (highlightColor.r) &&
        //        newColor.g == (highlightColor.g * intensity) &&
        //        newColor.b == (highlightColor.b * intensity))
        //    {
        //        bIncrementing = false;
        //        highlightStart = Time.time;
        //    }
        //}
        //else
        //{
            float elapsed = Time.time - highlightStart;
            float frac = Mathf.Min((elapsed / highlightDuration), 1f);

            renderer.material.SetColor("_Color", (highlightColor * intensity) * (1 - frac));

            if (frac >= 1)
                bHighlight = false;
        //}
    }
}
