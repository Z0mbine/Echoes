﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class patrollingNPC : MonoBehaviour {

    public GameObject target;
    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;
    private Animator anim;
    private AudioSource source;
    public AudioClip scream;
    Vector3 right;

    void Start()
    {
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).

        GotoNextPoint();
    }

    void Awake()
    {
        target = GameObject.FindWithTag("Player");
    }

    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;

        // transform.LookAt(points[destPoint].transform);
    }

    void Update()
    {
        Vector3 normal = (points[destPoint].position - transform.position).normalized;
        right = Vector3.Cross(normal, Vector3.up);

        transform.LookAt(transform.position + right);

        // Choose the next destination point when the agent gets
        // close to the current one.
        if (!agent.pathPending && agent.remainingDistance < 0.5f)
            GotoNextPoint();
    }
    
    
    void OnTriggerEnter(Collider collision)
    {
        if (!collision.gameObject.CompareTag("Player")) return;
        if (target == null) return;

        transform.LookAt(collision.transform.position + right);
        anim.SetBool("seenPlayer", true);
        agent.SetDestination(target.transform.position);
        source.PlayOneShot(scream, 1f);
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            anim.SetBool("seenPlayer", false);     
        }
    }
    /*
     void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            anim.Play("crawl");
        }
    }
    */
}