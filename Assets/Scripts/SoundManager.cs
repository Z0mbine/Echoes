﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    // Can't be set, only retrieved
    public static SoundManager Instance { get; private set; }

    public void Pause()
    {
        AudioListener.pause = true;
    }

    public void Unpause()
    {
        AudioListener.pause = false;
    }

    // Singleton our SoundManager
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this) // We already have a SoundManager present, destroy this crap
            Destroy(gameObject);

        // Don't destroy our GameManager if the level reloads
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {

	}
}
