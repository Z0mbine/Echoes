﻿using UnityEngine;

public class BasePlayer : MonoBehaviour
{
    public int ammo;
    private CollisionFlags colFlags;
    private float flStepSoundTime;
    [SerializeField] private AudioClip[] footstepSounds;
    [SerializeField] private AudioClip[] snapSounds;
    [SerializeField] private GameObject[] throwables;
    private int lastFoot;
    private int lastSnap;
    private bool m_bJumping;
    private float nextStep;
    private float nextStinger;
    private AudioSource sound;
    private CharacterController controller;
    [SerializeField] private Transform rightHand;
    private GameObject stick;

    // Use this for initialization
    private void Start()
    {
        sound = GetComponent<AudioSource>();
        controller = GetComponent<CharacterController>();
    }

    private void Awake()
    {
        //rightHand = GameObject.Find("hand_right");
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        var script = hit.gameObject.GetComponent<BaseProp>();

        if (!script) return;
        if (!script.canPickup) return;

        ammo++;
        Destroy(hit.gameObject);
    }

    /// <summary>
    ///     Pseudo-movement function. Checks if the OVR player is moving, and processes footsteps.
    /// </summary>
    private void HandlePlayerMove()
    {
        // Check if we've just landed after a jump
        if (m_bJumping && controller.isGrounded)
            OnPlayerHitGround();

        // Check for jumping
        if (OVRInput.GetDown(OVRInput.Button.One) && controller.isGrounded)
        {
            GetComponent<OVRPlayerController>().Jump();
            PlayFootstep();
            m_bJumping = true;
        }

        // Check if we should play a footstep
        var velocity = controller.velocity;
        HandleFootstep(Time.deltaTime, velocity.magnitude > 0f && controller.isGrounded);
    }

    private void OnPlayerHitGround()
    {
        m_bJumping = false;
        PlayFootstep();
    }

    /// <summary>
    ///     Routine to run every frame to determine what actions should be processed.
    /// </summary>
    private void HandlePlayerInput()
    {
        var bThrow = OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) || Input.GetMouseButtonDown(1);
        var bSnap = OVRInput.GetDown(OVRInput.Button.SecondaryThumbstick);

        // It's now beat-poetry simulator......
        if (bSnap) Snap();

        if (!bThrow || ammo <= 0) return;

        var projectile = Instantiate(Util.Choose(ref throwables));
        var handPos = rightHand.position;
        projectile.GetComponent<BaseProp>().canPickup = false;
        Physics.IgnoreCollision(projectile.GetComponent<MeshCollider>(), GetComponent<CharacterController>());
        projectile.transform.position = handPos;

        stick = projectile;

        var handAng = rightHand.rotation;
        var throwDir = handAng * Vector3.forward;
        var rigidBody = projectile.GetComponent<Rigidbody>();
        rigidBody.velocity = Vector3.zero;
        rigidBody.AddForce(throwDir * 15f, ForceMode.VelocityChange);
        rigidBody.AddTorque(handAng * Vector3.right * Random.Range(0.5f, 1.5f));
        rigidBody.AddTorque(handAng * Vector3.up * Random.Range(0.2f, 0.8f));

        ammo--;
    }

    /// <summary>
    ///     Produces a snap which will very briefly light up a large radius
    /// </summary>
    private void Snap()
    {
        Util.MakeSobelWave(rightHand.position, 65f, 400f, Color.white, 0f, 0.5f);

        int index;
        var snapClip = Util.Choose(ref snapSounds, out index);

        while (index == lastSnap) snapClip = Util.Choose(ref snapSounds, out index);
        lastSnap = index;

        AudioSource.PlayClipAtPoint(snapClip, rightHand.position, 0.65f);
    }

    private void FixedUpdate()
    {
        HandlePlayerMove();
    }

    // This doesn't run as fast as we need it to (and sometimes runs twice in a frame??), very useless
    private void Update()
    {
        HandlePlayerInput();

        //if (stick != null)
            //stick.transform.position = rightHand.position;

        if (Time.time < nextStinger) return;

        foreach (var enemy in FindObjectsOfType<GameObject>())
        {
            if (!enemy.CompareTag("Enemy")) continue;

            var center = enemy.transform.position + new Vector3(0f, 1f, 0f);

            if (Vector3.Distance(center, Camera.main.transform.position) >= 40f) continue;

            var screenPos = Camera.main.WorldToViewportPoint(center);

            if (!(screenPos.x >= 0) || !(screenPos.x <= 1) || !(screenPos.y >= 0) || !(screenPos.y <= 1) ||
                !(screenPos.z > 0)) continue;

            var dir = center - Camera.main.transform.position;
            dir.Normalize();

            var layerMask = 1 << 9; // we do not want to hit the player layer
            layerMask = ~layerMask;
            RaycastHit trace;
            var hit = Physics.SphereCast(
                Camera.main.transform.position,
                0.5f,
                dir,
                out trace,
                16000f,
                layerMask
            );

            if (!hit) return;

            if (!trace.collider.transform.root.CompareTag("Enemy")) continue;

            GameManager.Instance.PlayStinger();
            nextStinger = Time.time + 20f;
        }
    }

    private void PlayFootstep()
    {
        var rand = Random.Range(0, footstepSounds.Length);

        while (rand == lastFoot) rand = Random.Range(0, footstepSounds.Length);

        lastFoot = rand;

        sound.clip = footstepSounds[rand];
        sound.Play();

        Util.MakeSobelWave(transform.position, 20f, 70f, Color.white, 1f, 1.5f);
    }

    /// <summary>
    ///     Routine to run every frame which determines if a footstep should occur, and if so,
    ///     plays a footstep sound and corresponding effects.
    /// </summary>
    /// <param name="frameTime">The current frame time of the program</param>
    /// <param name="bMoving">Whether the calculations should have any effect.</param>
    private void HandleFootstep(float frameTime, bool bMoving)
    {
        if (!bMoving) return;

        if (controller.velocity.sqrMagnitude > 0f)
            flStepSoundTime += controller.velocity.magnitude * Time.fixedDeltaTime;

        if (flStepSoundTime <= nextStep)
            return;

        nextStep = flStepSoundTime + 1.5f;

        PlayFootstep();
    }
}