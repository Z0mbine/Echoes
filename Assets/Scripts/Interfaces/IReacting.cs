﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IReacting {

    void ReactSound(Vector3 position);
    void Find();
}
