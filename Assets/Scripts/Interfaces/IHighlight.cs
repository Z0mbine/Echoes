﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHighlight
{
    void Highlight(float duration, Color color);
}
