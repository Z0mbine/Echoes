﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * The main interface for actors
 * 
 * This is what will be retrieved via GetComponent<>() to access basic functions
 * such as Get/SetHealth, TakeDamage, and possibly others.
 * 
 * This will be implemented in things such as the player, NPCs, and breakable objects.
 * This will allow us to access specific methods without knowing what type of object it is.
 */

public interface IBreakable
{
    void TakeDamage(int amount);
    void Kill();

    int GetHealth();
    int GetMaxHealth();
    void SetHealth(int amount);
    void SetMaxHealth(int amount);
}
