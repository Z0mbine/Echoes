# ECHOES

An in-development horror game made with Unity and the Oculus SDK.
Soundwave-based navigation and visibility.

[Watch demo on YouTube](https://www.youtube.com/watch?v=jMHGaCGoPaU)

[View itch.io page](https://csumb.itch.io/echoes)